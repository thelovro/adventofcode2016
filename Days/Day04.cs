﻿namespace Days;
class Day04 : BaseDay
{
    public Day04()
    {
        SampleInputPartOne.Add(@"aaaaa-bbb-z-y-x-123[abxyz]
a-b-c-d-e-f-g-h-987[abcde]
not-a-real-room-404[oarel]
totally-real-room-200[decoy]","1514");

        SampleInputPartTwo.Add(@"qzmt-zixmtkozy-ivhz-343[zimth]", "very encrypted name");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int sectorIdSum = 0;
        foreach(string room in input)
        {
            sectorIdSum += GetSectorIdIfRealRoom(room);
        }

        return sectorIdSum.ToString();
    }

    private int GetSectorIdIfRealRoom(string room)
    {
        int indexBracket = room.IndexOf('[');
        int indexLastDash = room.LastIndexOf('-');
        string checksum = room.Substring(indexBracket + 1, 5);
        int sectorId = int.Parse(room.Substring(indexLastDash + 1, indexBracket - indexLastDash - 1));

        string letters = room.Substring(0, indexLastDash).Replace("-", string.Empty);
        var result = letters.AsEnumerable()
            .GroupBy(x => x)
            .Select(x => new { letter = x.Key, count = x.Count() })
            .OrderByDescending(x => x.count).ThenBy(x => x.letter)
            .Take(5).Select(x => x.letter);

        return string.Join("", result) == checksum ? sectorId : 0;
    }

    protected override string FindSecondSolution(string[] input)
    {
        foreach (string room in input)
        {
            int sectorId = GetSectorIdIfRealRoom(room);
            if (sectorId == 0) continue;

            string actualRoomName = DecriptRoomName(room, sectorId);

            if (IsTest)
                return actualRoomName;

            if (actualRoomName == "northpole object storage")
                return sectorId.ToString();
        }

        return "not yet";
    }

    private string DecriptRoomName(string room, int sectorId)
    {
        int indexLastDash = room.LastIndexOf('-');
        string letters = room.Substring(0, indexLastDash).Replace("-", " ");

        string actualRoomName = "";
        foreach (char c in letters)
        {
            if (c == ' ')
            {
                actualRoomName += " ";
                continue;
            }

            int id = (int)c - 97;
            id += sectorId;
            id = id % 26;
            id = id + 97;
            char newChar = (char)id;
            actualRoomName += newChar;
        }

        return actualRoomName;
    }
}