﻿namespace Days;
class Day03 : BaseDay
{
    public Day03()
    {
        SampleInputPartOne.Add(@"5 10 25
12 12 20
1 1 1", "2");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int sum = 0;
        foreach(string def in input)
        {
            sum += IsTriangle(def) ? 1 : 0;
        }

        return sum.ToString();
    }

    private bool IsTriangle(string def)
    {
        var parts = def.Split(' ');
        int a = int.Parse(parts[0]);
        int b = int.Parse(parts[1]);
        int c = int.Parse(parts[2]);

        return a + b > c && a + c > b && b + c > a;
    }

    protected override string FindSecondSolution(string[] input)
    {
        int sum = 0;
        for (int row = 0; row < input.Length; row += 3)
        {
            sum += IsTrianglePart2(input, row, 0) ? 1 : 0;
            sum += IsTrianglePart2(input, row, 1) ? 1 : 0;
            sum += IsTrianglePart2(input, row, 2) ? 1 : 0;
        }

        return sum.ToString();
    }

    private bool IsTrianglePart2(string[] input, int row, int column)
    {
        int a = int.Parse(input[row].Split(' ')[column]);
        int b = int.Parse(input[row + 1].Split(' ')[column]);
        int c = int.Parse(input[row + 2].Split(' ')[column]);

        return a + b > c && a + c > b && b + c > a;
    }
}