﻿namespace Days
{
    internal class Day13 : BaseDay
    {
        public Day13()
        {
            SampleInputPartOne.Add(@"10", "11");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            var targetCoordinate = IsTest ? (7, 4) : (31, 39);
            var start = (1, 1);
            int designersNumber = Convert.ToInt32(input[0]);

            Dictionary<(int x, int y), char> map = new();
            map[start] = GetMazeItem(0, 0, designersNumber);

            int steps = FindShortestRoute(start, targetCoordinate, map, designersNumber);

            return steps.ToString();
        }

        private int FindShortestRoute((int x, int y) start, (int x, int y) targetCoordinate, Dictionary<(int x, int y), char> map, int designersNumber)
        {
            Queue<((int x, int y) coordinates, int length)> queue = new();
            queue.Enqueue((start, 0));
            int minPathLength = int.MaxValue;

            HashSet<(int x, int y)> visited = new();

            while (queue.Count> 0)
            {
                var position = queue.Dequeue();
                if (visited.Contains(position.coordinates)) continue;
                visited.Add(position.coordinates);

                if (position.coordinates == targetCoordinate)
                {
                    if (position.length < minPathLength)
                    {
                        minPathLength = position.length;
                        break;
                    }
                }

                //go to all four directions
                GoToAllFourDirections(map, designersNumber, queue, position);
            }

            return minPathLength;
        }

        private void GoToAllFourDirections(Dictionary<(int x, int y), char> map, int designersNumber, Queue<((int x, int y) coordinates, int length)> queue, ((int x, int y) coordinates, int length) position)
        {
            //left
            var newPosition = (position.coordinates.x - 1, position.coordinates.y);
            if (CanGoToNewPosition(newPosition, map, designersNumber))
            {
                queue.Enqueue((newPosition, position.length + 1));
            }

            //right
            newPosition = (position.coordinates.x + 1, position.coordinates.y);
            if (CanGoToNewPosition(newPosition, map, designersNumber))
            {
                queue.Enqueue((newPosition, position.length + 1));
            }

            //up
            newPosition = (position.coordinates.x, position.coordinates.y - 1);
            if (CanGoToNewPosition(newPosition, map, designersNumber))
            {
                queue.Enqueue((newPosition, position.length + 1));
            }

            //down
            newPosition = (position.coordinates.x, position.coordinates.y + 1);
            if (CanGoToNewPosition(newPosition, map, designersNumber))
            {
                queue.Enqueue((newPosition, position.length + 1));
            }
        }

        private bool CanGoToNewPosition((int x, int y) newPosition, Dictionary<(int x, int y), char> map, int designersNumber)
        {
            if (newPosition.x < 0 || newPosition.y < 0) return false;
            if (!map.ContainsKey(newPosition))
            {
                map[newPosition] = GetMazeItem(newPosition.x, newPosition.y, designersNumber);
            }

            return map[newPosition] == '.';
        }

        private char GetMazeItem(int x, int y, int number)
        {
            var value = x * x + 3 * x + 2 * x * y + y + y * y + number;
            int numberOfOnes = Convert.ToString(value, 2).Replace("0", "").Length;
            
            return numberOfOnes % 2 == 0 ? '.' : '#';
        }

        protected override string FindSecondSolution(string[] input)
        {
            var targetCoordinate = IsTest ? (7, 4) : (31, 39);
            var start = (1, 1);
            int designersNumber = Convert.ToInt32(input[0]);

            Dictionary<(int x, int y), char> map = new();
            map[start] = GetMazeItem(0, 0, designersNumber);

            int steps = FindDistinctLocations(start, targetCoordinate, map, designersNumber);

            return steps.ToString();
        }

        private int FindDistinctLocations((int x, int y) start, (int x, int y) targetCoordinate, Dictionary<(int x, int y), char> map, int designersNumber)
        {
            Queue<((int x, int y) coordinates, int length)> queue = new();
            queue.Enqueue((start, 0));
            
            HashSet<(int x, int y)> visited = new();

            while (queue.Count > 0)
            {
                var position = queue.Dequeue();
                if (visited.Contains(position.coordinates)) continue;
                visited.Add(position.coordinates);

                
                if (position.length ==50)
                {
                    continue;
                }

                //go to all four directions
                GoToAllFourDirections(map, designersNumber, queue, position);
            }

            return visited.Count;
        }
    }
}