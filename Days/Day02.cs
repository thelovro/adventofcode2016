﻿namespace Days;
class Day02 : BaseDay
{
    public Day02()
    {
        SampleInputPartOne.Add(@"ULL
RRDDD
LURDL
UUUUD", "1985");

        SampleInputPartTwo.Add(@"ULL
RRDDD
LURDL
UUUUD", "5DB3");
    }

    protected override string FindFirstSolution(string[] input)
    {
        string code = "";
        (int, int) position = (1,1);

        foreach(string instruction in input)
        {
            foreach(char c in instruction)
            {
                if (c == 'R' && position.Item1 < 2) position.Item1++;
                if (c == 'D' && position.Item2 < 2) position.Item2++;
                if (c == 'L' && position.Item1 > 0) position.Item1--;
                if (c == 'U' && position.Item2 > 0) position.Item2--;
            }

            code += (position.Item2 * 3 + position.Item1 + 1).ToString();
        }

        return code;
    }

    protected override string FindSecondSolution(string[] input)
    {
        string code = "";
        (int, int) position = (0, 2);

        foreach (string instruction in input)
        {
            foreach (char c in instruction)
            {
                if (c == 'R')
                {
                    if(position.Item2 == 1 && position.Item1 < 3
                        || position.Item2 == 2 && position.Item1 < 4
                        || position.Item2 == 3 && position.Item1 < 3)
                        position.Item1++;
                }
                if (c == 'D')
                {
                    if(position.Item1 == 1 && position.Item2 < 3
                        || position.Item1 == 2 && position.Item2 < 4
                        || position.Item1 == 3 && position.Item2 < 3)
                    position.Item2++;
                }
                if (c == 'L')
                {
                    if (position.Item2 == 1 && position.Item1 > 1
                        || position.Item2 == 2 && position.Item1 > 0
                        || position.Item2 == 3 && position.Item1 > 1)
                        position.Item1--;
                }
                if (c == 'U')
                {
                    if (position.Item1 == 1 && position.Item2 > 1
                        || position.Item1 == 2 && position.Item2 > 0
                        || position.Item1 == 3 && position.Item2 > 1)
                        position.Item2--;
                }
            }

            code += ReturnCode(position);
        }

        return code;
    }

    private string ReturnCode((int, int) position)
    {
        if (position.Item2 == 0)
            return "1";
        if(position.Item2 == 1)
            return (1 + position.Item1).ToString();
        if(position.Item2 == 2)
            return (5 + position.Item1).ToString();
        if (position.Item2 == 3)
        {
            if (position.Item1 == 1) return "A";
            if (position.Item1 == 2) return "B";
            if (position.Item1 == 3) return "C";
        }
        if (position.Item2 == 4)
            return "D";

        return "dafaq";
    }       
}