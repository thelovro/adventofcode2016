﻿namespace Days;
class Day05 : BaseDay
{
    public Day05()
    {
        SampleInputPartOne.Add(@"abc", "18F47A30");

        SampleInputPartTwo.Add(@"abc", "05ACE8E3");
    }

    protected override string FindFirstSolution(string[] input)
    {
        long index = 0;
        string password = "";
        while (password.Length < 8)
        {
            string inputValue = $"{input[0]}{index++}";
            string hash = CalculateHash(inputValue);

            if (hash.TrimStart('0').Length <= hash.Length - 5)
            {
                password += hash[5];
            }
        }

        //takes about 22s to find the solution
        return password;
    }

    private static string CalculateHash(string inputValue)
    {
        string hash;
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(inputValue);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }

            hash = sb.ToString();
        }

        return hash;
    }

    protected override string FindSecondSolution(string[] input)
    {
        long index = 0;
        string[] password = new string[8];
        while (password.Any(c => c == null))
        {
            string inputValue = $"{input[0]}{index++}";
            string hash = CalculateHash(inputValue);

            //if (!hash.StartsWith("00000") || hash[5] < 48 || hash[5] > 55)
            if ((hash.TrimStart('0').Length > hash.Length - 5) || hash[5] < 48 || hash[5] > 55)
            {
                continue;
            }

            int passwordIndex = Convert.ToInt32(hash[5].ToString());
            if (password[passwordIndex] == null)
            {
                password[passwordIndex] = hash[6].ToString();
            }
        }

        //takes about 40-70s to find the solution
        return string.Join("", password);
    }
}