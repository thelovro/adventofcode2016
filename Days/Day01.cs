﻿namespace Days;
class Day01 : BaseDay
{
    public Day01()
    {
        SampleInputPartOne.Add(@"R2, L3", "5");
        SampleInputPartOne.Add(@"R2, R2, R2", "2");
        SampleInputPartOne.Add(@"R5, L5, R5, R3", "12");

        SampleInputPartTwo.Add(@"R8, R4, R4, R8", "4");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int x = 0;
        int y = 0;
        Direction currentDirection = Direction.Up;

        foreach (string instruction in input[0].Split(", "))
        {
            currentDirection = Turn(currentDirection, instruction.Substring(0, 1) == "R" ? 1 : -1);
            int length = int.Parse(instruction.Substring(1, instruction.Length - 1));

            if (currentDirection == Direction.Up) y += length;
            else if (currentDirection == Direction.Right) x += length;
            else if (currentDirection == Direction.Down) y -= length;
            else if (currentDirection == Direction.Left) x -= length;
        }

        return (Math.Abs(x) + Math.Abs(y)).ToString();
    }

    enum Direction
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3
    }

    private Direction Turn(Direction currentDirection, int instruction)
    {
        int dir = (int)currentDirection + instruction;

        if (dir < 0)
            return Direction.Left;
        else if (dir > 3)
            return Direction.Up;

        return (Direction)dir;
    }

    protected override string FindSecondSolution(string[] input)
    {
        int x = 0;
        int y = 0;
        Direction currentDirection = Direction.Up;
        List<(int, int)> locations = new List<(int, int)>();
        locations.Add((x, y));
            
        foreach (string instruction in input[0].Split(", "))
        {
            currentDirection = Turn(currentDirection, instruction.Substring(0, 1) == "R" ? 1 : -1);
            int length = int.Parse(instruction.Substring(1, instruction.Length - 1));
                
            for (int i = 0; i < length; i++)
            {
                if (currentDirection == Direction.Up)
                {
                    y++;
                }
                else if (currentDirection == Direction.Right)
                {
                    x++;
                }
                else if (currentDirection == Direction.Down)
                {
                    y--;
                }
                else if (currentDirection == Direction.Left)
                {
                    x--;
                }

                if (locations.Contains((x, y)))
                    return (Math.Abs(x) + Math.Abs(y)).ToString();

                locations.Add((x, y));
            }
        }

        return "the f***??";
    }
}