﻿namespace Days;
class Day07 : BaseDay
{
    public Day07()
    {
        SampleInputPartOne.Add(@"abba[mnop]qrst
abcd[bddb]xyyx
aaaa[qwer]tyui
ioxxoj[asdfgh]zxcvbn", "2");

        SampleInputPartTwo.Add(@"aba[bab]xyz
xyx[xyx]xyx
aaa[kek]eke
zazbz[bzb]cdb", "3");
    }

    protected override string FindFirstSolution(string[] input)
    {
        Regex r = new Regex(@"[a-z]+");

        int counter = 0;
        foreach(var line in input)
        {
            var matches = r.Matches(line);

            bool containsAbba = false;
            for (int i = 0; i < matches.Count; i+=2)
            {
                containsAbba |= DoesContainAbba(matches[i].Value);
            }

            bool containsAbbaWithinBrackets = false;
            for (int i = 1; i < matches.Count; i += 2)
            {
                containsAbbaWithinBrackets |= DoesContainAbba(matches[i].Value);
            }

            if(containsAbba && !containsAbbaWithinBrackets)
            {
                counter++;
            }
        }

        return counter.ToString();
    }

    private bool DoesContainAbba(string input)
    {
        for (int i = 0; i < input.Length-3; i++)
        {
            if (input[i] == input[i + 3] 
                && input[i + 1] == input[i + 2]
                && input[i] != input[i+1])
                return true;
        }

        return false;
    }

    private List<string> GetAbas(string input, bool invert)
    {
        List<string> abas = new();
        for (int i = 0; i < input.Length - 2; i++)
        {
            if (input[i] == input[i + 2]
                && input[i] != input[i + 1])
            {
                if (invert)
                {
                    abas.Add(input[i + 1].ToString() + input[i].ToString() + input[i + 1].ToString());
                }
                else
                {
                    abas.Add(input.Substring(i, 3));
                }
            }
        }

        return abas;
    }

    protected override string FindSecondSolution(string[] input)
    {
        Regex r = new Regex(@"[a-z]+");

        int counter = 0;
        foreach(var line in input)
        {
            var matches = r.Matches(line);

            List<string> abas = new();
            for (int i = 0; i < matches.Count; i+=2)
            {
                abas = abas.Union(GetAbas(matches[i].Value, false)).ToList();
            }

            List<string> abasInBrackets = new();
            for (int i = 1; i < matches.Count; i += 2)
            {
                abasInBrackets = abasInBrackets.Union(GetAbas(matches[i].Value, true)).ToList();
            }

            if(abas.Intersect(abasInBrackets).Count() > 0)
            {
                counter++;
            }
        }

        return counter.ToString();
    }
}