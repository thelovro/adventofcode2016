﻿namespace Days;
class Day10 : BaseDay
{
    public Day10()
    {
        SampleInputPartOne.Add(@"value 5 goes to bot 2
bot 2 gives low to bot 1 and high to bot 0
value 3 goes to bot 1
bot 1 gives low to output 1 and high to bot 0
bot 0 gives low to output 2 and high to output 0
value 2 goes to bot 2", "2");

        SampleInputPartTwo.Add(@"", "");
    }

    protected override string FindFirstSolution(string[] input)
    {
        Dictionary<int, List<int>> bots = ParseBots(input);
        Dictionary<int, Instruction> botsInstructions = ParseBotInstructions(input);
        Dictionary<int, List<int>> outputs = new();

        int lowValue = IsTest ? 2 : 17;
        int highValue = IsTest ? 5 : 61;

        int finalBot = -1;
        while(bots.Any(b=>b.Value.Count == 2))
        {
            var bot = bots.First(b => b.Value.Count == 2);
            if (bot.Value.Contains(lowValue) && bot.Value.Contains(highValue))
            {
                finalBot = bot.Key;
                break;
            }

            PassMicrochips(bots, outputs, botsInstructions, bot);

            bot.Value.Clear();
        }

        return finalBot.ToString();
    }

    private static void PassMicrochips(Dictionary<int, List<int>> bots, Dictionary<int, List<int>> outputs, Dictionary<int, Instruction> botsInstructions, KeyValuePair<int, List<int>> bot)
    {
        if (botsInstructions[bot.Key].BotLowId >= 0)
        {
            AddMicrochipToTarget(bots, botsInstructions, botsInstructions[bot.Key].BotLowId, bot.Value.Min());
        }

        if (botsInstructions[bot.Key].BotHighId >= 0)
        {
            AddMicrochipToTarget(bots, botsInstructions, botsInstructions[bot.Key].BotHighId, bot.Value.Max());
        }

        if (botsInstructions[bot.Key].OutputLowId >= 0)
        {
            AddMicrochipToTarget(outputs, botsInstructions, botsInstructions[bot.Key].OutputLowId, bot.Value.Min());
        }

        if (botsInstructions[bot.Key].OutputHighId >= 0)
        {
            AddMicrochipToTarget(outputs, botsInstructions, botsInstructions[bot.Key].OutputHighId, bot.Value.Max());
        }
    }

    private static void AddMicrochipToTarget(Dictionary<int, List<int>> bots, Dictionary<int, Instruction> botsInstructions, int botId, int microchip)
    {
        if (bots.ContainsKey(botId))
        {
            bots[botId].Add(microchip);
        }
        else
        {
            bots.Add(botId, new() { microchip });
        }
    }

    private Dictionary<int, Instruction> ParseBotInstructions(string[] input)
    {
        Dictionary<int, Instruction> bots = new();

        Regex r = new Regex("bot (?<bot>[0-9]+) gives low to (?<targetLow>[a-z]+) (?<targetLowId>[0-9]+) and high to (?<targetHigh>[a-z]+) (?<targetHighId>[0-9]+)");
        foreach (string line in input)
        {
            if (!r.IsMatch(line))
            {
                continue;
            }

            int bot = Convert.ToInt32(r.Match(line).Groups["bot"].Value);
            string targetLow = r.Match(line).Groups["targetLow"].Value;
            int targetLowId = Convert.ToInt32(r.Match(line).Groups["targetLowId"].Value);
            string targetHigh = r.Match(line).Groups["targetHigh"].Value;
            int targetHighId = Convert.ToInt32(r.Match(line).Groups["targetHighId"].Value);

            int botLowId = targetLow == "bot" ? targetLowId : -1;
            int outputLowId = targetLow != "bot" ? targetLowId : -1;
            int botHighId = targetHigh == "bot" ? targetHighId : -1;
            int outputHighId = targetHigh != "bot" ? targetHighId : -1;

            bots.Add(bot, new Instruction(bot, botLowId, outputLowId, botHighId, outputHighId));
        }

        return bots;
    }

    public record Instruction(int BotId, int BotLowId, int OutputLowId, int BotHighId, int OutputHighId);

    private Dictionary<int, List<int>> ParseBots(string[] input)
    {
        Dictionary<int, List<int>> bots = new();

        Regex r = new Regex("value (?<value>[0-9]+) goes to bot (?<bot>[0-9]+)");
        foreach(string line in input)
        {
            if(!r.IsMatch(line))
            {
                continue;
            }

            int bot = Convert.ToInt32(r.Match(line).Groups["bot"].Value);
            int value = Convert.ToInt32(r.Match(line).Groups["value"].Value);

            if (!bots.ContainsKey(bot))
            {
                bots.Add(bot, new());
            }

            bots[bot].Add(value);
        }

        return bots;
    }

    protected override string FindSecondSolution(string[] input)
    {
        Dictionary<int, List<int>> bots = ParseBots(input);
        Dictionary<int, Instruction> botsInstructions = ParseBotInstructions(input);
        Dictionary<int, List<int>> outputs = new();

        while (bots.Any(b => b.Value.Count == 2))
        {
            var bot = bots.First(b => b.Value.Count == 2);
            PassMicrochips(bots, outputs, botsInstructions, bot);

            bot.Value.Clear();
        }

        int solution = outputs[0].Sum() * outputs[1].Sum() * outputs[2].Sum();

        return solution.ToString();
    }
}