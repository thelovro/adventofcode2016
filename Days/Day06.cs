﻿namespace Days;
class Day06 : BaseDay
{
    public Day06()
    {
        SampleInputPartOne.Add(@"eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar", "easter");

        SampleInputPartTwo.Add(@"eedadn
drvtee
eandsr
raavrd
atevrs
tsrnev
sdttsa
rasrtv
nssdts
ntnada
svetve
tesnvt
vntsnd
vrdear
dvrsen
enarar", "advent");
    }

    protected override string FindFirstSolution(string[] input)
    {
        List<Dictionary<char, int>> items = CalculateCharacterFrequency(input);
        var frequentCharacters = items.Select(i => i.Where(c => c.Value == i.Max(m => m.Value)).Select(s => s.Key).First());

        return string.Join("", frequentCharacters);
    }

    private List<Dictionary<char, int>> CalculateCharacterFrequency(string[] input)
    {
        List<Dictionary<char, int>> items = new();
        for (int i = 0; i < input[0].Length; i++)
        {
            items.Add(new Dictionary<char, int>());
        }

        foreach(string line in input)
        {
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (items[i].ContainsKey(c))
                {
                    items[i][c]++;
                }
                else
                {
                    items[i].Add(c, 1 );
                }
            }
        }

        return items;
    }

    protected override string FindSecondSolution(string[] input)
    {
        List<Dictionary<char, int>> items = CalculateCharacterFrequency(input);
        var frequentCharacters = items.Select(i => i.Where(c => c.Value == i.Min(m => m.Value)).Select(s => s.Key).First());

        return string.Join("", frequentCharacters);
    }
}