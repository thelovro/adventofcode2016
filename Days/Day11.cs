﻿using System.ComponentModel;
using System.Reflection.Emit;

namespace Days;

internal class Day11 : BaseDay
{
    public Day11()
    {
        SampleInputPartOne.Add(@"The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
The second floor contains a hydrogen generator.
The third floor contains a lithium generator.
The fourth floor contains nothing relevant.", "11");
    }

    enum Floor
    {
        First,
        Second,
        Third,
        Fourth,
    }

    protected override string FindFirstSolution(string[] input)
    {
        //return "temp";
        Dictionary<string, Floor> items = ParseInitalState(input);

        Floor elevatorFloor = Floor.First;
        Dictionary<string, int> positionsHash = new();
        int solution = MoveThings(items, elevatorFloor);
        
        return solution.ToString();
    }

    private int MoveThings(Dictionary<string, Floor> inputItems, Floor inputElevatorFloor)
    {
        Queue<(Dictionary<string, Floor> items, int steps, Floor elevatorFloor)> q = new();
        Dictionary<string, int> positionsHash = new();
        q.Enqueue((inputItems, 0, inputElevatorFloor));

        int minSteps = int.MaxValue;

        while (q.Count > 0)
        {
            //speed optimization
            if (q.Count > 80000)
            {
                var x = q.OrderByDescending(a => a.items.Where(i=>i.Value == Floor.First).Count()*1 
                + a.items.Where(i => i.Value == Floor.Second).Count() * 2
                + a.items.Where(i => i.Value == Floor.Third).Count() * 3
                + a.items.Where(i => i.Value == Floor.Fourth).Count() * 4)
                    .Take(5000)
                    .Distinct()
                    .ToList();
                q.Clear();

                foreach (var qItem in x)
                {
                    q.Enqueue(qItem);
                }
            }

            var item = q.Dequeue();

            if (item.steps > minSteps)
                continue;

            if (!item.items.Any(x => x.Value != Floor.Fourth))
            {
                if (item.steps < minSteps)
                    minSteps = item.steps;
                continue;
            }

            string hash = CalculateHash(item.items, item.elevatorFloor);
            if (positionsHash.ContainsKey(hash))
            {
                if (item.steps >= positionsHash[hash])
                    continue;
            }
            else
            {
                positionsHash.Add(hash, item.steps);
            }

            if (!IsValidState(item.items, item.elevatorFloor))
            {
                continue;
            }

            foreach (var option in GetElevatorPermutations(item.items, item.elevatorFloor))
            {
                Floor floor;
                if (item.elevatorFloor > Floor.First)
                {
                    floor = (Floor)Enum.ToObject(typeof(Floor), item.elevatorFloor - 1);
                    var itemsTmp = item.items.ToDictionary(entry => entry.Key, entry => entry.Value);
                    itemsTmp[option.Item1] = floor;
                    if (option.Item2 != null)
                    {
                        itemsTmp[option.Item2] = floor;
                    }

                    q.Enqueue((itemsTmp, item.steps + 1, item.elevatorFloor -1));
                }
                if (item.elevatorFloor < Floor.Fourth)
                {
                    floor = (Floor)Enum.ToObject(typeof(Floor), item.elevatorFloor + 1);
                    var itemsTmp = item.items.ToDictionary(entry => entry.Key, entry => entry.Value);
                    itemsTmp[option.Item1] = floor;
                    if (option.Item2 != null)
                    {
                        itemsTmp[option.Item2] = floor;
                    }

                    q.Enqueue((itemsTmp, item.steps + 1, item.elevatorFloor + 1));
                }
            }
        }

        return minSteps;
    }

    private Dictionary<string, Floor> ParseInitalState(string[] input)
    {
        Dictionary<string, Floor> items = new();
        for (int i = 0; i < input.Length; i++)
        {
            string row = input[i];
            if (row.EndsWith("contains nothing relevant.")) continue;

            var rowItems = row
                .Substring(row.IndexOf(" a ") + 3)
                .TrimEnd('.')
                .Replace( ", a ", ",").Replace(", an ", ",").Replace(", and a ", "," ).Replace(" and a ", ",")
                .Split(',');
            
            foreach (var item in rowItems)
            {
                string initials = string.Join("-", item.Split(' ').AsEnumerable().Select(x => x.Substring(0, 2)));
                items.Add(initials, (Floor)Enum.ToObject(typeof(Floor), i));
            }
        }

        return items;
    }

    private bool IsValidState(Dictionary<string, Floor> items, Floor elevatorFloor)
    {
        for(int i=0; i<4; i++)
        {
            if (!items.Any(x => (int)x.Value == i)) continue;

            if (!items.Any(x => (int)x.Value == i && x.Key.Split('-')[1] == "mi")) continue;
            if (!items.Any(x => (int)x.Value == i && x.Key.Split('-')[1] == "ge")) continue;

            foreach (var microchip in items.Where(x => (int)x.Value == i && x.Key.Split('-')[1] == "mi"))
            {
                if(!items.Any(x => (int)x.Value == i && x.Key == microchip.Key.Split('-')[0]+ "-ge"))
                {
                    return false;
                }
            }
        }

        return true;
    }

    private string CalculateHash(Dictionary<string, Floor> items, Floor elevatorFloor)
    {
        return string.Join("_", items.Select(x => x.Key + x.Value.ToString())) + "_" + elevatorFloor;
    }

    private IEnumerable<(string, string)> GetElevatorPermutations(Dictionary<string, Floor> items, Floor elevatorFloor)
    {
        List<(string, string)> permutations = new();
        var tmpItems = items.Where(x => x.Value == elevatorFloor);
        for (int i = 0; i < tmpItems.Count(); i++)
        {
            permutations.Add((tmpItems.ElementAt(i).Key, null));

            for (int j = i + 1; j < tmpItems.Count(); j++)
            {
                permutations.Add((tmpItems.ElementAt(i).Key, tmpItems.ElementAt(j).Key));
            }
        }

        return permutations;
    }

    protected override string FindSecondSolution(string[] input)
    {
        input[0] = input[0].Replace(".",", an elerium generator, an elerium-compatible microchip, a dilithium generator, a dilithium-compatible microchip.");

        Dictionary<string, Floor> items = ParseInitalState(input);

        Floor elevatorFloor = Floor.First;
        Dictionary<string, int> positionsHash = new();
        int solution = MoveThings(items, elevatorFloor);

        //77
        return solution.ToString();
    }
}