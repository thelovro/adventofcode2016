﻿namespace Days;
class Day09 : BaseDay
{
    public Day09()
    {
        SampleInputPartOne.Add(@"ADVENT", "6");
        SampleInputPartOne.Add(@"A(1x5)BC", "7");
        SampleInputPartOne.Add(@"(3x3)XYZ", "9");
        SampleInputPartOne.Add(@"A(2x2)BCD(2x2)EFG", "11");
        SampleInputPartOne.Add(@"(6x1)(1x3)A", "6");
        SampleInputPartOne.Add(@"X(8x2)(3x3)ABCY", "18");

        SampleInputPartTwo.Add(@"(3x3)XYZ", "9");
        SampleInputPartTwo.Add(@"X(8x2)(3x3)ABCY", "20");
        SampleInputPartTwo.Add(@"(27x12)(20x12)(13x14)(7x10)(1x12)A", "241920");
        SampleInputPartTwo.Add(@"(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN", "445");
    }

    protected override string FindFirstSolution(string[] input)
    {
        string finalString = DecompressSequence(input[0]);

        return finalString.Length.ToString();
    }

    private string DecompressSequence(string sequence)
    {
        string finalString = "";
        for (int i = 0; i < sequence.Length; i++)
        {
            if (sequence[i] == '(')
            {
                int closingBracketIndex = sequence.IndexOf(')', i);
                string sequenceDef = sequence.Substring(i + 1, closingBracketIndex - i - 1);
                int numChars = Convert.ToInt32(sequenceDef.Split('x')[0]);
                int numRepetitions = Convert.ToInt32(sequenceDef.Split('x')[1]);

                string chars = sequence.Substring(closingBracketIndex + 1, numChars);
                for (int x = 0; x < numRepetitions; x++)
                {
                    finalString += chars;
                }

                i = closingBracketIndex + numChars;
            }
            else
            {
                finalString += sequence[i];
            }
        }

        return finalString;
    }

    protected override string FindSecondSolution(string[] input)
    {
        long length = DecompressSequenceVersionTwo(input[0]);

        return length.ToString();
    }

    private long DecompressSequenceVersionTwo(string sequence)
    {
        long length = 0;
        for (int i = 0; i < sequence.Length; i++)
        {
            if (sequence[i] == '(')
            {
                int closingBracketIndex = sequence.IndexOf(')', i);
                string sequenceDef = sequence.Substring(i + 1, closingBracketIndex - i - 1);
                int numChars = Convert.ToInt32(sequenceDef.Split('x')[0]);
                int numRepetitions = Convert.ToInt32(sequenceDef.Split('x')[1]);

                long subSequenceLength = DecompressSequenceVersionTwo(sequence.Substring(closingBracketIndex + 1, numChars));
                length += subSequenceLength * numRepetitions;

                i = closingBracketIndex + numChars;
            }
            else
            {
                length++;
            }
        }

        return length;
    }
}