﻿namespace Days;
class Day08 : BaseDay
{
    public Day08()
    {
        SampleInputPartOne.Add(@"rect 3x2
rotate column x=1 by 1
rotate row y=0 by 4
rotate column x=1 by 1", "6");
    }

    protected override string FindFirstSolution(string[] input)
    {
        int displayWidth = IsTest ? 7 : 50;
        int displayHeight = IsTest ? 3 : 6;

        string[,] image = RunCode(input, displayWidth, displayHeight);

        var z = string.Join("", image.OfType<string>());

        return z.Length.ToString();
    }

    private string[,] RunCode(string[] input, int displayWidth, int displayHeight)
    {
        Regex rect = new Regex(@"rect (?<x>\d+)x(?<y>\d+)");
        Regex rotateColumn = new Regex(@"rotate column x=(?<x>\d+) by (?<size>\d+)");
        Regex rotateRow = new Regex(@"rotate row y=(?<y>\d+) by (?<size>\d+)");

        string[,] image = new string[displayWidth, displayHeight];
        foreach(string line in input)
        {
            if(rect.IsMatch(line))
            {
                var match = rect.Match(line);
                for (int x = 0; x < Convert.ToInt32(match.Groups["x"].Value); x++)
                {
                    for (int y = 0; y < Convert.ToInt32(match.Groups["y"].Value); y++)
                    {
                        image[x, y] = "#";
                    }
                }
            }
            else if (rotateColumn.IsMatch(line))
            {
                var match = rotateColumn.Match(line);
                string[,] tmpImage = (string[,])image.Clone();
                int column = Convert.ToInt32(match.Groups["x"].Value);
                for (int y = 0; y < displayHeight; y++)
                {
                    int newRow = (y + Convert.ToInt32(match.Groups["size"].Value)) % displayHeight;
                    tmpImage[column, newRow] = image[column, y];
                }

                image = tmpImage;
            }
            else if (rotateRow.IsMatch(line))
            {
                var match = rotateRow.Match(line);
                string[,] tmpImage = (string[,])image.Clone();
                int row = Convert.ToInt32(match.Groups["y"].Value);
                for (int x = 0; x < displayWidth; x++)
                {
                    int newColumn = (x + Convert.ToInt32(match.Groups["size"].Value)) % displayWidth;
                    tmpImage[newColumn, row] = image[x, row];
                }

                image = tmpImage;
            }
        }

        return image;
    }

    protected override string FindSecondSolution(string[] input)
    {
        int displayWidth = IsTest ? 7 : 50;
        int displayHeight = IsTest ? 3 : 6;

        string[,] image = RunCode(input, displayWidth, displayHeight);

        for (int y = 0; y < displayHeight; y++)
        {
            for (int x = 0; x < displayWidth; x++)
            {
                Console.Write(string.IsNullOrEmpty(image[x, y]) ? " " : image[x, y]);
            }
            Console.WriteLine();
        }

        return "EOARGPHYAO";
    }
}