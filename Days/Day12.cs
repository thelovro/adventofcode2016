﻿namespace Days
{
    internal class Day12 : BaseDay
    {
        public Day12()
        {
            SampleInputPartOne.Add(@"cpy 41 a
inc a
inc a
dec a
jnz a 2
dec a", "42");

            SampleInputPartTwo.Add(@"", "");
        }

        protected override string FindFirstSolution(string[] input)
        {
            int[] registers = new int[4] { 0, 0, 0, 0 };
            RunOperations(input, registers);

            return registers[0].ToString();
        }

        private static void RunOperations(string[] input, int[] registers)
        {
            for (int i = 0; i < input.Length; i++)
            {
                string line = input[i];
                string operation = line.Split(' ')[0];
                string parameterOne = line.Split(' ')[1];
                string parameterTwo = "";
                if (line.Split(' ').Length == 3)
                    parameterTwo = line.Split(' ')[2];

                if (operation == "cpy")
                {
                    int value;
                    if (parameterOne[0] >= 97) //register
                        value = registers[parameterOne[0] - 97];
                    else
                        value = int.Parse(parameterOne);

                    registers[parameterTwo[0] - 97] = value;
                }
                else if (operation == "inc")
                {
                    registers[parameterOne[0] - 97] += 1;
                }
                else if (operation == "dec")
                {
                    registers[parameterOne[0] - 97] -= 1;
                }
                else if (operation == "jnz")
                {
                    int value;
                    if (parameterOne[0] >= 97) //register
                        value = registers[parameterOne[0] - 97];
                    else
                        value = int.Parse(parameterOne);

                    if (value != 0)
                    {
                        i += (int.Parse(parameterTwo) - 1);
                    }
                }
            }
        }

        protected override string FindSecondSolution(string[] input)
        {
            int[] registers = new int[4] { 0, 0, 1, 0 };
            RunOperations(input, registers);

            return registers[0].ToString();
        }
    }
}